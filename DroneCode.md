#Drone Mode Opertion

**Drone Mode Operation** is designed to search for enemies and eliminate the *target*. The program works by searching for *target*
and verifying that the *target* is not team mate. If the *target* is a team mate then the drone will move to a new location and
search for a new *target*. Once a *target* has been confirmed as an **enemy** and the path is clear then the drone will take a
shot if the target is in range. If the *target* is not in range the drone will move and recheck pathing for the shot. If the shot
hits the **enemy** then the **enemy** is destoyed. If the shot is a miss the drone will move to a new location and repeat the
process.

###Uploaded to BitBucket on May 23 2018