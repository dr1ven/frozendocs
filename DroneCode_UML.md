#Drone Battle


###Actors
* Drone 1
* Drone 2

###Description
**Drone 1** will caclulate it's current position. It will then determine the position of **Drone 2**. Once it determines the location of **Drone 2** it will take a shot. If the shot does not hit, **Drone 1** will move position and **Drone 2** will evade. Then **Drone 1** will fire from the new position and repeat the process until the shot hits. If the shot does hit, **Drone 2** is destroyed, and **Drone 1** will search for a new target. 