![Photo by UnpopularToTheMax](https://farm3.staticflickr.com/3893/14607399140_73a413f1b6_o.jpg)
�New Armored Security Vehicle� by Georgia National Guard is licensed under CC BY 2.0

# Project FROZENFIST
## VortexOps

Design software for **semi-autonomous armored assault vehicles**. The vehicles are designed to operate either remotely with a human pilot or in *"robot mode"* with guidance from an on-board **Artificial Intelligence** (AI) system.

Contact: [Craig Suhrke](mailto:craig.suhrke@smail.rasmussen.edu)

#### Pushed to BitBucket Repository - May 17th, 2018

### Modified README on BitBucket - May 24th, 2018